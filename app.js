var express = require('express');
var app = express();
const port = 3000

/**
 * This function received a name as string and return them a welcome message
 * @param {String} name 
 * @returns {String}
 */
const welcome = (name) => {
    return "Hello " + name;
}

/* GET home page. */
app.get('/getUser',  function(req, res, next) {
  res.status(200).send(welcome(res))
});
